###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02c - catNmouse - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a catNmouse program
###
### @author  Mariko Galton <mgalton@hawaii.edu>
### @date    27_Jan_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = catNmouse

all: $(TARGET)

catNmouse: catNmouse.c
	$(CC) $(CFLAGS) -o $(TARGET) catNmouse.c
clean:
	rm -f $(TARGET) *.o

