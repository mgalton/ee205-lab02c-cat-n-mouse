///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Mariko Galton <mgalton@hawaii.edu>
/// @date    27_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DEFAULT_MAX_NUMBER (2048)

int main(int argc,char* argv[]) {
   srand(time(0));   
   int theNumberImThinkingof =(rand() % DEFAULT_MAX_NUMBER) + 1;
   
   do {
      printf("Ok cat, I'm thinking of a number from 1 to %d. Make a guess:  ", DEFAULT_MAX_NUMBER);
   
      int aGuess;
      scanf( "%d", &aGuess );

      if (aGuess < 1) {
          printf("You must enter a number that's >= 1\n");
      }

      if (aGuess > DEFAULT_MAX_NUMBER) {
            printf("You must enter a number that's <= %d\n", DEFAULT_MAX_NUMBER);
      }

      if (aGuess > theNumberImThinkingof) {
          printf("No cat... the number I'm thinking of is smaller than %d\n", aGuess);
      }  
       
      if (aGuess < theNumberImThinkingof) {
          printf("No cat...the number I'm thinking of is larger than %d\n", aGuess);
      }

      if (aGuess == theNumberImThinkingof) {
          printf("You got me.\n");
          printf("\n"
            "    /\\_____/\\\n"
            "   /  o   o  \\\n"
            "  ( ==  ^  == )\n"
            "   )         (\n"
            "  (           )\n"
            " ( (  )   (  ) )\n"
            "(__(__)___(__)__)\n ");
          printf("\n");
             return 0;
      }
      
   } while (1); 

   return 1;  
   
   }

